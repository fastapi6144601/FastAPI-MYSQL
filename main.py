import models

from routes import routes
from fastapi import FastAPI
from database import engine

app = FastAPI()
models.Base.metadata.create_all(bind=engine)

app.include_router(routes)
