import models

from typing import Annotated
from database import SessionLocal
from sqlalchemy.orm import Session
from schemas import UserBase, PostBase
from fastapi import APIRouter, HTTPException, Depends, status

def get_db():
  db = SessionLocal()
  try:
    yield db
  finally:
    db.close

db_dependency = Annotated[Session, Depends(get_db)] 

routes = APIRouter()
@routes.get("/users", status_code=status.HTTP_200_OK)
async def list_user(db: db_dependency):
  users = db.query(models.User).all()
  if not users:
    raise HTTPException(status_code=404, detail='User was not found')
  return users

@routes.get("/users/{user_id}", status_code=status.HTTP_200_OK)
async def get_user(user_id: int, db: db_dependency):
  user = db.query(models.User).filter(models.User.id == user_id).first()
  if not user:
    raise HTTPException(status_code=404, detail='User was not found')
  return user

@routes.put("/users/{user_id}", status_code=status.HTTP_200_OK)
async def put_user(user_id: int, user: UserBase, db: db_dependency):
  db_user = db.query(models.User).filter(models.User.id == user_id).first()
  if not user:
    raise HTTPException(status_code=404, detail='User was not found')
  db_user.username = user.username
  db.commit()
  return user.username

@routes.post("/users/", status_code=status.HTTP_201_CREATED)
async def create_user(user: UserBase, db: db_dependency):
  db_user = models.User(**user.dict())
  db.add(db_user)
  db.commit()
  return "User was created"


@routes.get("/posts", status_code=status.HTTP_200_OK)
async def list_post(db: db_dependency):
  posts = db.query(models.Post).all()
  if not posts:
    raise HTTPException(status_code=404, detail='Post was not found')
  return posts

@routes.get("/posts/{post_id}", status_code=status.HTTP_200_OK)
async def get_post(post_id: int, db: db_dependency):
  post = db.query(models.Post).filter(models.Post.id == post_id).first()
  if not post:
    raise HTTPException(status_code=404, detail='Post was not found')
  return post

@routes.put("/posts/{post_id}", status_code=status.HTTP_200_OK)
async def put_post(post_id: int, post:PostBase, db: db_dependency):
  db_post = db.query(models.Post).filter(models.Post.id == post_id).first()
  if not db_post:
    raise HTTPException(status_code=404, detail='Post was not found')
  db_post.title = post.title
  db_post.content = post.content
  db_post.user_id = post.user_id
  db.commit()
  return db_post

@routes.post("/posts/", status_code=status.HTTP_201_CREATED)
async def create_post(post: PostBase, db: db_dependency):
  db_post = models.Post(**post.dict())
  db.add(db_post)
  db.commit()
  return "Post was created"

@routes.delete("/posts/{post_id}", status_code=status.HTTP_200_OK)
async def delete_post(post_id: int, db: db_dependency):
  result = db.query(models.Post).filter(models.Post.id == post_id).first()
  if result is None:
    raise HTTPException(status_code=404, detail='Post was not found')
  db.delete(result)
  db.commit()
  return "Post was deleted"
