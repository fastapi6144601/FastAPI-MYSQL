from database import Base
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String, ForeignKey

class User(Base):
  __tablename__ = 'tb_users'

  id = Column(Integer, primary_key=True, index=True)
  username = Column(String(50), unique=True)

  posts = relationship("Post", back_populates="user")

class Post(Base):
  __tablename__ = 'tb_posts'

  id = Column(Integer, primary_key=True, index=True)
  title = Column(String(50))
  content = Column(String(150))
  user_id = Column(Integer, ForeignKey('tb_users.id'))

  user = relationship("User", back_populates="posts")